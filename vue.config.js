module.exports = {
  publicPath: process.env.NODE_ENV === 'production'
    ? '/~cs62160292/learn_bootstrap/'
    : '/'
}
